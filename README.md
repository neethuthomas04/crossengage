**CROSS ENGAGE CODE CHALLENGE**

This repo provides the basic solution for the "CROSS ENGAGE" code challenge

###### Challenge - 1
Batching
Assume you have a dataset of events that can be represented as json objects as follows:

{"timestamp": 1, "user_id": 1, "event": "Pageview")
{"timestamp": 1, "user_id": 2, "event": "Add to Cart")
{"timestamp": 2, "user_id": 1, "event": "Page View")
...
Given a file (check batching-dataset.ndjson) containing every event seen for a given day, write a script/class/application that reads this file and generates as output a file containing every user session on that file. For simplicity, please assume the number of events fits on the memory of a single computer.

###### Challenge - 2
Streaming
Assume you have the same dataset as before, but now coming from a kafka topic instead.

Given this topic containing events still being generated, write a script/class/application that reads messages from this topic and writes a file containing every user session on that file. For simplicity, please assume the number of concurrent users will be small enough to fit on the memory of a single computer.

---

## Solutions 

Note : For both the solutions, Spark Scala is used because of its dynamic memory management capabilities in standalone or cluster mode and unified rich APIs for both batch and stream processing. 

For this challenge, A single system is used with below configuration
---
###### CPU : 3.0 Ghz Quard core processor
###### Memory : 12 gb
###### OS : Ubuntu 18.04
###### Scala : 2.11
###### Spark : 2.4.4
###### Kafka : 2.12 / kafka 0.10 libraries for spark kafka streaming
###### Java  : JDK 8.0 
---
#### Challenge - 1
###### Script : https://bitbucket.org/neethuthomas04/crossengage/src/master/src/batch_scala_script_v1.scala

This is a initial version but fully functional spark scala script that calculates user sessions for "m" intervals / day.

###### Steps to Run
1. Setup the spark 2.4.4 or above 
```
https://medium.com/devilsadvocatediwakar/installing-apache-spark-on-ubuntu-8796bfdd0861
```
2. Clone the repo
```
git clone https://neethuthomas04@bitbucket.org/neethuthomas04/crossengage.git
```
3. Run the below command
```
spark-shell --master local
```
4. Modify the input, output paths, and m (session interval) in the script src/batch_scala_script_v1.scala 
5. Paste the script on the spark-shell, script takes approx 1 to 2 minutes to process to complete.
6. Here is the sample input and output for a single user with m = 30 minutes (1800 secs), detailed output is present in output/batch/ with day partitioned 
```
input  :
{"user_id": 26, "timestamp": 1493768693, "event_name": "event_06"}
{"user_id": 26, "timestamp": 1493768700, "event_name": "event_04"}
{"user_id": 26, "timestamp": 1493768728, "event_name": "event_04"}
{"user_id": 26, "timestamp": 1493768747, "event_name": "event_00"}
{"user_id": 26, "timestamp": 1493768792, "event_name": "event_08"}
{"user_id": 26, "timestamp": 1493768798, "event_name": "event_04"}
{"user_id": 26, "timestamp": 1493770890, "event_name": "event_06"}
{"user_id": 26, "timestamp": 1493770909, "event_name": "event_02"}
{"user_id": 26, "timestamp": 1493770941, "event_name": "event_03"}
{"user_id": 26, "timestamp": 1493777867, "event_name": "event_04"}
{"user_id": 26, "timestamp": 1493777906, "event_name": "event_03"}
{"user_id": 26, "timestamp": 1493777926, "event_name": "event_05"}
{"user_id": 26, "timestamp": 1493777960, "event_name": "event_03"}
{"user_id": 26, "timestamp": 1493777976, "event_name": "event_05"}
{"user_id": 26, "timestamp": 1493777996, "event_name": "event_05"}
{"user_id": 26, "timestamp": 1493778017, "event_name": "event_04"}
{"user_id": 26, "timestamp": 1493778041, "event_name": "event_02"}

output :
{"user_id":26,"event_count":6,"session_starttimestamp":1493768693,"session_endtimestamp":1493770493,"session":[{"event_name":"event_06","timestamp":1493768693},{"event_name":"event_04","timestamp":1493768700},{"event_name":"event_04","timestamp":1493768728},{"event_name":"event_00","timestamp":1493768747},{"event_name":"event_08","timestamp":1493768792},{"event_name":"event_04","timestamp":1493768798}]}
{"user_id":26,"event_count":3,"session_starttimestamp":1493770890,"session_endtimestamp":1493772690,"session":[{"event_name":"event_06","timestamp":1493770890},{"event_name":"event_02","timestamp":1493770909},{"event_name":"event_03","timestamp":1493770941}]}
{"user_id":26,"event_count":8,"session_starttimestamp":1493777867,"session_endtimestamp":1493779667,"session":[{"event_name":"event_04","timestamp":1493777867},{"event_name":"event_03","timestamp":1493777906},{"event_name":"event_05","timestamp":1493777926},{"event_name":"event_03","timestamp":1493777960},{"event_name":"event_05","timestamp":1493777976},{"event_name":"event_05","timestamp":1493777996},{"event_name":"event_04","timestamp":1493778017},{"event_name":"event_02","timestamp":1493778041}]}
```
---
#### Challenge - 2
###### Script : https://bitbucket.org/neethuthomas04/crossengage/src/master/src/stream_scala_script_v1.scala

This is a initial version but fully functional spark scala script that calculates user sessions for "m" intervals / day.

###### Steps to Run
1. Setup the spark 2.4.4 or above 
```
https://medium.com/devilsadvocatediwakar/installing-apache-spark-on-ubuntu-8796bfdd0861
```
2. Clone the repo
```
git clone https://neethuthomas04@bitbucket.org/neethuthomas04/crossengage.git
```
3. Setup the Kafka and start kafka server
```
a) wget http://apache.lauf-forum.at/kafka/2.3.0/kafka_2.11-2.3.0.tgz
b) tar -zxvf kafka_2.11-2.3.0.tgz
c) mv kafka_2.11-2.3.0 /usr/local/kafka_2.12-2.3.0
d) cd /usr/local/kafka_2.12-2.3.0
e) bin/zookeeper-server-start.sh config/zookeeper.properties
f) bin/kafka-server-start.sh config/server.properties
g) bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic crossengage
h) cat cross-engage/challenge-sessions/input/batching-dataset.ndjson | bin/kafka-console-producer.sh --broker-list localhost:9092 --topic crossengage
i) bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic crossengage --from-beginning

```
4. Run the below command
```
spark-shell --packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.4.4
```
5. Modify the output path, and m (session interval) in the script src/stream_scala_script_v1.scala 
5. Paste the script on the spark-shell, script takes approx 1 to 2 minutes to process to complete.
6. Here is the sample input and output for a single user with m = 30 minutes (1800 secs), detailed output is present in output/stream/ with day partitioned 
```
input  :
{"user_id": 5, "timestamp": 1493767750, "event_name": "event_02"}
{"user_id": 5, "timestamp": 1493767761, "event_name": "event_06"}
{"user_id": 5, "timestamp": 1493767778, "event_name": "event_07"}
{"user_id": 5, "timestamp": 1493768984, "event_name": "event_01"}
{"user_id": 5, "timestamp": 1493768994, "event_name": "event_01"}
{"user_id": 5, "timestamp": 1493769018, "event_name": "event_00"}
{"user_id": 5, "timestamp": 1493772970, "event_name": "event_01"}
{"user_id": 5, "timestamp": 1493772981, "event_name": "event_07"}
{"user_id": 5, "timestamp": 1493783681, "event_name": "event_09"}
{"user_id": 5, "timestamp": 1493788099, "event_name": "event_02"}
{"user_id": 5, "timestamp": 1493788126, "event_name": "event_05"}

output :
{"user_id":5,"event_count":6,"session_starttimestamp":1493767750,"session_endtimestamp":1493769550,"session":[{"event_name":"event_02","timestamp":1493767750},{"event_name":"event_06","timestamp":1493767761},{"event_name":"event_07","timestamp":1493767778},{"event_name":"event_01","timestamp":1493768984},{"event_name":"event_01","timestamp":1493768994},{"event_name":"event_00","timestamp":1493769018}]}
{"user_id":5,"event_count":2,"session_starttimestamp":1493772970,"session_endtimestamp":1493774770,"session":[{"event_name":"event_01","timestamp":1493772970},{"event_name":"event_07","timestamp":1493772981}]}
{"user_id":5,"event_count":1,"session_starttimestamp":1493783681,"session_endtimestamp":1493785481,"session":[{"event_name":"event_09","timestamp":1493783681}]}
{"user_id":5,"event_count":2,"session_starttimestamp":1493788099,"session_endtimestamp":1493789899,"session":[{"event_name":"event_02","timestamp":1493788099},{"event_name":"event_05","timestamp":1493788126}]}
```
---