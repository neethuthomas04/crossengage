import org.apache.spark.sql.expressions.Window

val input_path = "/home/neethu/Documents/code/cross-engage/challenge-sessions/input"
val output_path = "/home/neethu/Documents/code/cross-engage/challenge-sessions/output"
val m = 1800 // Session in milliseconds for better control on the session

val input_df = spark.read.json(input_path)
val input_cleaned_df = input_df.drop("_corrupt_record")

val window = Window.partitionBy($"user_id").orderBy($"timestamp" asc).rangeBetween(-1 * m,0)

val main_df =  input_cleaned_df.
withColumn("event_count_per_session", count($"user_id").over(window)).
withColumn("session_ranking",first($"timestamp").over(window))
				
val session_df = main_df.groupBy("user_id","session_ranking").
agg(collect_list(struct($"event_name",$"timestamp")) as "session",
	max($"event_count_per_session") as "event_count").
withColumnRenamed("session_ranking","session_starttimestamp").
withColumn("session_endtimestamp",$"session_starttimestamp" + lit(m))

val final_df = session_df.select("user_id","event_count","session_starttimestamp","session_endtimestamp","session").
orderBy("session_starttimestamp")

final_df.show

final_df.repartition(1).
withColumn("partition_col",from_unixtime($"session_starttimestamp","yyyy-MM-dd")).
write.partitionBy("partition_col").
mode("overwrite").
json(output_path)