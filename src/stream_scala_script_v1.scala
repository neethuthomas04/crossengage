//spark-shell --packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.4.4

import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.streaming.kafka010._
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.DataFrame

val streaming_window_time = 10 //secs
val topics = "crossengage"
val brokers = "localhost:9092"
val groupId = "crossengagesession"

val m = 1800 //secs

val conf = new SparkConf().setMaster("local[2]").setAppName("crossengagesession")
val ssc = new StreamingContext(spark.sparkContext, Seconds(streaming_window_time))

val topicsSet = topics.split(",").toSet
val kafkaParams = Map[String, Object](
ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> brokers,
ConsumerConfig.GROUP_ID_CONFIG -> groupId,
ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
"auto.offset.reset" -> "earliest",
"enable.auto.commit" -> (false: java.lang.Boolean)
)
val messages = KafkaUtils.createDirectStream[String, String](
ssc,
LocationStrategies.PreferConsistent,
ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams))

val lines = messages.map(_.value)
//val cnt = lines.count
//cnt.print
lines.foreachRDD(rdd => {
    val event_df = spark.read.json(rdd)
    event_df.repartition(1).write.mode("append").json("/home/neethu/Documents/code/cross-engage/challenge-sessions/stream/temp")
})

 // Start the computation
ssc.start()
ssc.awaitTermination()